import { Router } from "express";
import Controller from "../src/core/Controller";

describe("Controller", () => {
  describe("#setup", () => {
    it("should raise no entrypoint error", (done) => {
      try {
        Controller.setup(null, Router());

        done("Should expect an error");
      } catch {
        done();
      }
    });

    it("should raise no router error", (done) => {
      try {
        Controller.setup("/", null);

        done("Should expect an error");
      } catch {
        done();
      }
    });
  });
});
