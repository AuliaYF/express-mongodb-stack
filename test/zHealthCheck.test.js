import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import app from "../src/server";

chai.use(chaiHttp);
chai.should();

describe("API: /health-check", () => {
  describe("GET /health-check", () => {
    it("should report server's health", (done) => {
      chai
        .request(app)
        .get("/health-check")
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.database).to.be.equal("connected");

          done();
        });
    });
  });
});
