import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import app from "../src/server";

chai.use(chaiHttp);
chai.should();

describe("API: /test", () => {
  describe("POST /test", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .post("/test")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });

  describe("GET /test", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .get("/test")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });

  describe("GET /test/new", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .get("/test/new")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });

  describe("GET /test/:id", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .get("/test/1")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });

  describe("GET /test/1/edit", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .get("/test/1/edit")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });

  describe("PUT /test/1", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .put("/test/1")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });

  describe("DELETE /test/1", () => {
    it("should raise not implemented error", (done) => {
      chai
        .request(app)
        .delete("/test/1")
        .end((err, res) => {
          res.should.have.status(501);

          done();
        });
    });
  });
});
