import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import app from "../src/server";

chai.use(chaiHttp);
chai.should();

describe("API: /", () => {
  describe("GET /", () => {
    it("should return 'Hello, world!'", (done) => {
      chai
        .request(app)
        .get("/")
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.message).to.be.equal("Hello, world!");

          done();
        });
    });
  });
});
