# Express MongoDB Backend Stack

[![pipeline status](https://gitlab.com/AuliaYF/express-mongodb-stack/badges/master/pipeline.svg)](https://gitlab.com/AuliaYF/express-mongodb-stack/-/commits/master) [![coverage report](https://gitlab.com/AuliaYF/express-mongodb-stack/badges/master/coverage.svg)](https://gitlab.com/AuliaYF/express-mongodb-stack/-/commits/master)

## Run (via docker-compose)

```powershell
$ docker-compose up -d --build # http://localhost:4001/
```

## Run (via yarn/npm !!Development Only!!)

Yarn:

```powershell
$ yarn start # start with nodemon & babel-node
```

npm:

```powershell
$ npm run start # start with nodemon & babel-node
```

## Build (via yarn/npm)

Yarn:

```powershell
$ yarn build
$ yarn serve # http://localhost:4001/
```

npm:

```powershell
$ npm run build
$ npm run serve # http://localhost:4001/
```
