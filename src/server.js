import { json } from "body-parser";
import cors from "cors";
import express, { urlencoded } from "express";
import * as database from "./config/database";
import routes from "./config/routes";

database.connect();

const app = express();

const port = process.env.PORT || 8080;

app.use(urlencoded({ extended: true }));
app.use(json());
app.use(cors());

app.get("/", (req, res) => {
  res.status(200).json({
    message: "Hello, world!",
  });
});

app.use("/", routes);

app.get("/health-check", async (req, res) => {
  let health = {
    database: database.state(),
  };

  res.status(200).json(health);
});

app.listen(port, function () {
  console.info("API", `server listening on port ${port}!`);
});

export default app;
