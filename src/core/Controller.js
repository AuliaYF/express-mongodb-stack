import { Router } from "express";

export default class BaseController {
  constructor() {
    this.router = Router();

    this.router.post("/", this.create);
    this.router.get("/", this.index);
    this.router.get("/new", this.new);
    this.router.get("/:id", this.show);
    this.router.get("/:id/edit", this.edit);
    this.router.put("/:id", this.update);
    this.router.delete("/:id", this.delete);
  }

  static setup(entrypoint, router) {
    if (!entrypoint) throw new Error("No entrypoint provided");

    if (!router) throw new Error("No router provided");

    const controller = new this();

    router.use(entrypoint, controller.router);
  }

  index(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'index' action not implemented yet",
    });
  }

  show(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'show' action not implemented yet",
    });
  }

  new(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'new' action not implemented yet",
    });
  }

  create(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'create' action not implemented yet",
    });
  }

  edit(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'edit' action not implemented yet",
    });
  }

  update(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'update' action not implemented yet",
    });
  }

  delete(req, res, next) {
    res.status(501).json({
      type: "failed",
      message: "'delete' action not implemented yet",
    });
  }
}
