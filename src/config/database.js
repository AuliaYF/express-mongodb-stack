import mongoose from "mongoose";
import { config } from "dotenv";
import _ from "lodash";

config();

mongoose.Promise = global.Promise;

const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB,
} = process.env;

const options = {
  authSource: "admin",
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
  connectTimeoutMS: 10000,
};

const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;

const connectionState = {
  0: "disconnected",
  1: "connected",
  2: "connecting",
  3: "disconnecting",
};

export const connect = () => mongoose.connect(url, options);

export const state = () =>
  _.get(connectionState, mongoose.connection.readyState, "unknown");
