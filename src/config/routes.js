import { Router } from "express";
import TestController from "../controllers/Test";

const router = Router();

TestController.setup("/test", router);

export default router;
