FROM node:12.18.1-alpine

RUN mkdir -p /home/node/app/node_modules
RUN mkdir -p /home/node/app/dist
RUN chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package.json ./

USER node

RUN yarn install

COPY --chown=node:node . /home/node/app

RUN yarn build

EXPOSE 8080

EXPOSE 27017

CMD [ "yarn", "serve"]